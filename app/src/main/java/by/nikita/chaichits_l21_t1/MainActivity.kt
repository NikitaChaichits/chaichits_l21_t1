package by.nikita.chaichits_l21_t1

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import by.nikita.chaichits_l21_t1.rvAdapter.ColorItem
import by.nikita.chaichits_l21_t1.rvAdapter.ColorRVAdapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ColorContract.View, SwipeRefreshLayout.OnRefreshListener {


    lateinit var items: ArrayList<ColorItem>
    lateinit var adapter: ColorRVAdapter
    lateinit var mPresenter: ColorContract.Presenter

    val mListColors = listOf("Red", "Pink", "Green", "Blue", "Orange", "Yellow", "Black", "Grey")
    //    val mListColorsHex = listOf(R.color.red, R.color.pink, R.color.green, R.color.blue,
//        R.color.orange, R.color.yellow, R.color.black, R.color.grey
//    )
    val mListColorsHex = listOf(
        "#FF0000", "#ff69b4", "#00FF00", "#0000FF",
        "#ffa500", "#FFFF00", "#000000", "#d3d3d3"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mPresenter = ColorPresenter(this)

        showListColors()

        swipe_container.setOnRefreshListener {
            Handler().postDelayed({
                showListColors()
                swipe_container.isRefreshing = false
            }, 1000)
        }
    }

    override fun showListColors() {
        items = mPresenter.generateListColors(mListColors, mListColorsHex, 10)
        adapter = ColorRVAdapter(items)
        rv.adapter = adapter
    }

    override fun onRefresh() {
        swipe_container.isRefreshing = false
    }
}
