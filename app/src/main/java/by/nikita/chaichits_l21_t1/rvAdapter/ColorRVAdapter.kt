package by.nikita.chaichits_l21_t1.rvAdapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.nikita.chaichits_l21_t1.R


class ColorRVAdapter(initColorItems: List<ColorItem>) :
    RecyclerView.Adapter<ColorRVAdapter.ViewHolder>() {

    val colorItems: ArrayList<ColorItem> = ArrayList()

    init {
        colorItems.addAll(initColorItems)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.adapter_item, viewGroup, false)
        return ViewHolder(v)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(ViewHolder: ViewHolder, i: Int) {
        ViewHolder.color.setBackgroundColor(Color.parseColor(colorItems[i].сolor))
        ViewHolder.nameColor.text = colorItems[i].nameColor
    }

    override fun getItemCount(): Int {
        return colorItems.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var color: ImageView
        var nameColor: TextView

        init {
            this.color = itemView.findViewById(R.id.ivColor)
            this.nameColor = itemView.findViewById(R.id.tvColor)
        }
    }
}