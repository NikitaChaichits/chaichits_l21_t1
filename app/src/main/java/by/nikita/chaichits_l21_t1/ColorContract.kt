package by.nikita.chaichits_l21_t1

import by.nikita.chaichits_l21_t1.rvAdapter.ColorItem


interface ColorContract {

    interface View {
        fun showListColors()
    }

    interface Presenter {
        fun generateListColors(
            mListColors: List<String>,
            mListColorsHex: List<String>,
            count: Int
        ): ArrayList<ColorItem>
    }
}