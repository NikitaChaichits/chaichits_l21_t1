package by.nikita.chaichits_l21_t1

import by.nikita.chaichits_l21_t1.rvAdapter.ColorItem
import kotlin.random.Random

class ColorPresenter : ColorContract.Presenter {

    private var mColor: ColorItem
    private var mView: ColorContract.View

    constructor(mView: ColorContract.View) {
        this.mColor = ColorItem()
        this.mView = mView
    }

    override fun generateListColors(
        mListColors: List<String>,
        mListColorsHex: List<String>,
        count: Int
    ): ArrayList<ColorItem> {
        var list = ArrayList<ColorItem>()

        for (i in 1..count) {
            var position = Random.nextInt(mListColors.size)
            list.add(ColorItem(mListColorsHex[position], mListColors[position]))
        }

        return list
    }

}